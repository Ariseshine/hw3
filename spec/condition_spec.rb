require_relative '../lib/condition.rb'

RSpec.describe Condition do
	new_html = "<div class='a-column a-span3 olpConditionColumn' role='gridcell'><div id='offerCondition' class='a-section a-spacing-small'><span id='olpNew' class='a-size-medium olpCondition a-text-bold'>New</span></div></div>"
  used_good_html = "<div id='offerCondition' class='a-section a-spacing-small'><span id='olpUsed' class='a-size-medium olpCondition a-text-bold'>Used-<span id='offerSubCondition'>Good</span></span></div>"
  used_very_good_html = "<div id='offerCondition' class='a-section a-spacing-small'><span id='olpUsed' class='a-size-medium olpCondition a-text-bold'>Used-<span id='offerSubCondition'>Very Good</span></span></div>"
  used_like_new_html = "<div id='offerCondition' class='a-section a-spacing-small'><span id='olpUsed' class='a-size-medium olpCondition a-text-bold'>Used-<span id='offerSubCondition'>Like New</span></span></div>"
  used_acceptable = "<div id='offerCondition' class='a-section a-spacing-small'><span id='olpUsed' class='a-size-medium olpCondition a-text-bold'>Used-<span id='offerSubCondition'>Acceptable</span></span></div>"

  html_arr = [new_html, used_like_new_html, used_very_good_html, used_good_html, used_acceptable]
  context_arr = ["New", "Used - Like New", "Used - Very Good", "Used - Good", "Used - Acceptable"]

  new_vals = [true, false, false, false, false]
  used_vals = [false, true, true, true, true]
  conditions = ["NEW", "USED", "USED", "USED", "USED"]
  sub_conditions = ["NEW", "LIKE NEW", "VERY GOOD", "GOOD", "ACCEPTABLE"]

	html_arr.each_with_index do |v, i|
    context v do
			before(:example) do
				html = html_arr[i]
        @cond = Condition.new(html)
				@cond.parse
			end

			it "#new? should return #{new_vals[i]}" do
				expect(@cond.new?).to eq(new_vals[i])
      end

			it "#used? should return #{used_vals[i]}" do
				expect(@cond.used?).to eq(used_vals[i])
			end

			it "condition should be #{conditions[i]}" do
				expect(@cond.condition.upcase).to eq(conditions[i])
			end

			it "sub_condition should be #{sub_conditions[i]}" do
				expect(@cond.sub_condition.upcase).to eq(sub_conditions[i])
			end

    end
  end
end