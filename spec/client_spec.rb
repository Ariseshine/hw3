require_relative '../lib/client.rb'

RSpec.describe Client do
	it "should return '12323'" do
		http_client = double('http_client')
		allow(http_client).to receive(:get) {'12323'}
		client = Client.new(http_client, "tsst")
		expect(client.html).to eq('12323')
	end
end