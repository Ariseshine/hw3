require 'nokogiri'

class Price

	attr_reader :price_html, :parse_shipping_fee

	def initilaize(price_html)
		@price_html = price_html
	end

	def parse(price_html)
		doc = Nokogiri::HTML(price_html)
		sale_price = doc.at_css('.olpOfferPrice')
		shipping_fee = doc.at_css('.olpShippingPrice')
		@sale_price = Price.parse_sale_price(sale_price)
		@parse_shipping_fee = Price.parse_shipping_fee(shipping_fee)
	end

	def self.parse_sale_price(price_html)
		doc = Nokogiri::HTML(price_html)
		sale_price = doc.at_css('.olpOfferPrice')

		sale_price.text.gsub(/[^0-9\.]/, '').to_f
	end

	def self.parse_shipping_fee(price_html)
		doc = Nokogiri::HTML(price_html)
		shipping_fee = doc.at_css('.olpShippingPrice')
		if shipping_fee
			shipping_fee.text.gsub(/[^0-9\.]/, '').to_f
		else
			0
		end

	end

end

