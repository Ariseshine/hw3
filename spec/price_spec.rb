require_relative '../lib/price.rb'
RSpec.describe Price do
	# price_html_block = "<div class='a-column a-span2 olpPriceColumn' role='gridcell'><span class='a-size-large a-color-price olpOfferPrice a-text-bold'>$39.24</span><p class='olpShippingInfo'><span class='a-color-secondary'>&amp; <b>FREE Shipping</b></span></p></div>"
	price_html_block = "<div class='a-column a-span2 olpPriceColumn' role='gridcell'><span class='a-size-large a-color-price olpOfferPrice a-text-bold'>$55.94</span><p class='olpShippingInfo'><span class='a-color-secondary'><span class='olpShippingPrice'>$3.99</span><span class='olpShippingPriceText'>shipping</span></span></p></div>"
	free_shipping_html_block = "<div class='a-column a-span2 olpPriceColumn' role='gridcell'><span class='a-size-large a-color-price olpOfferPrice a-text-bold'>$49.45</span><p class='olpShippingInfo'><span class='a-color-secondary'>&amp; <b>FREE Shipping</b></span></p></div>"


	describe ".parse_seller_price" do
		it "shold get sale price" do
			price = Price.parse_sale_price(price_html_block)
			expect(price).to eq(55.94)
		end

		it "shold get sale price" do
			price = Price.parse_sale_price(free_shipping_html_block)
			expect(price).to eq(49.45)
		end
	end

	describe ".parse_shipping_fee" do
		context 'parse raw html string' do
			it 'should work on none free shipping' do
				shipping_fee = Price.parse_shipping_fee(price_html_block)
				expect(shipping_fee).to eq(3.99)
			end

			it 'should work on free shipping' do
				shipping_fee = Price.parse_shipping_fee(free_shipping_html_block)
				expect(shipping_fee).to eq(0)
			end
		end
	end
end